\selectlanguage{dutch}
\section{Unit externe sensoren: Mux}
\label{sec:mux_detail}
De unit mux is een verzameling van de 64 analoge signalen die gesampled worden door 4 multiplexers. 
Voor deze unit wordt de Blue Pill gebruikt om de ADC-signalen te lezen en de 74HC4067 \cite{web_hc4067} 
gebruikt om te multiplexen. De Blue Pill heeft een maximale ADC sample snelheid van 14 MHz bij de klokfrequentie
van 20 MHz. Omdat bij het switchen van kanalen op de multiplexer deels afhankelijk is van de Blue Pill klokfrequentie,
is hiervoor de klokfrequentie van 64 MHz gekozen. De ADC sample snelheid is hierdoor 10 MHz geworden.
In deze beschrijving zal er een uitleg plaats vinden, waarbij een multiplexer van 16:1 kanalen gebruikt wordt.

\subsection{STM32CubeIDE configuratie}
Het ADC-module van de Blue Pill heeft meerdere kanalen. Het is mogelijk om bijvoorbeeld alleen het eerste kanaal te gebruiken
voor de eerste 16 caliper sensoren. De rest van de kanalen kunnen dan over de overige hoeksensoren worden aangewezen. Door 
de optie 'Number Of Conversion' op 2 te zetten, wordt hiermee aangegeven dat we 2 kanalen willen gebruiken op een ADC-module. 
Als volgt kunnen we de totale ADC Conversion Time instellen. Dit is de tijd bij het overschakelen naar een andere ADC-kanaal.
De formule van deze tijd is hieronder weergegeven:
\\
\[TConv = Sampling Time (cycles) + 12,5 cycles\] 
\\
De sampling time is in cycles. Dit wil aangeven hoeveel cycles we op een signaal willen sampelen. Als de sampling time van
1,5 cycles gekozen wordt waarbij de ADC-klokfrequentie gelijk is aan 14 MHz. Dan is de conversion time gelijk aan:
\begin{equation}
    Tconv = 1,5 + 12,5 = 14\ cycles
    \frac{14}{14.000.000} = 1\ us
\end{equation}
De conversie tijd kan als input worden gebruikt bij de formule van de ADC sampling frequentie:
\[SamplingRate = \frac{1}{Tconv}\] 
Als we de bekende waardes invullen dan krijgen we:
\begin{equation}
    \frac{1}{1u} = 1Ms/sec
\end{equation}
De instelling van het ADC-module moeten identiek zijn aan \autoref{fig:adc_ch}.
\begin{figure}[ht!]
    \includegraphics[scale=0.8]{Detailontwerp/Onderdelen/Afbeelding/MUX/adc_config.png}
    \caption{Het instellen van de ADC kanalen}
    \label{fig:adc_ch}
\end{figure}
\\
Ook worden 4 GPIO pinnen als output ingesteld, zodat de kanalen van de multiplexer geselecteerd kunnen worden. De 4
GPIO pinnen zijn gekozen op basis van de hardware. Deze 4 pinnen zijn naast elkaar gevestigd zie \autoref{fig:gpio_mux}.
\begin{figure}[ht!]
    \includegraphics[scale=0.5]{Detailontwerp/Onderdelen/Afbeelding/MUX/mux_pinconfig.png}
    \caption{De 4 GPIO pinnen als output voor het selecteren van kanalen}
    \label{fig:gpio_mux}
\end{figure} 

\subsection{Softwarecode}
De struct van de unit mux is hieronder weergegeven. De variabel valueMux1 heeft een type uint16\_t omdat het signaal
een waarde heeft van 12-bit.
\\\\
\begin{adjustbox}{width=0.4\textwidth}
    \color{Green}
    \begin{lstlisting}
typedef struct{
    uint16_t valueMux1[16];
} Mux;

Mux mux;
    \end{lstlisting}
\end{adjustbox}
\\\\
Eerder is benoemd dat alleen het eerste kanaal gebruikt zal worden om de eerste 16 waardes van de hoeksensoren in te lezen.
De Blue Pill zal normaal gesproken automatisch zelf kiezen welk kanaal dat zal doen, maar dit kan ook handmatig zijn.
Door in de functie van 'MX\_ADC1\_Init' de onderstaande code te commenteren, kunnen we een functie creëren waarbij 
het kanaal gekozen wordt. 
\\\\
\begin{adjustbox}{width=\textwidth}
    \color{Green}
    \begin{lstlisting}
/** Configure for the selected ADC regular channel its 
corresponding rank in the sequencer and its sample time.
*/
sConfig.Channel = ADC_CHANNEL_0;
sConfig.Rank = 1;
sConfig.SamplingTime = ADC_SAMPLETIME_28CYCLES;
if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
{
Error_Handler();
}
/** Configure for the selected ADC regular channel its 
corresponding rank in the sequencer and its sample time.
*/
sConfig.Channel = ADC_CHANNEL_1;
sConfig.Rank = 2;
sConfig.SamplingTime = ADC_SAMPLETIME_84CYCLES;
if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
{
Error_Handler();
}
/** Configure for the selected ADC regular channel its 
corresponding rank in the sequencer and its sample time.
*/
sConfig.Channel = ADC_CHANNEL_TEMPSENSOR;
sConfig.Rank = 3;
sConfig.SamplingTime = ADC_SAMPLETIME_112CYCLES;
if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
{
Error_Handler();
}
    \end{lstlisting}
\end{adjustbox}
\\\\
De code hierboven configureert de kanalen van de ADC. Als we deze code in een nieuwe functie stoppen, dan kunnen we 
een specifiek kanaal van de ADC aanroepen. Let wel op dat dit altijd gebeuren moet, als de microcontroller opnieuw, of 
een andere peripherals bijgevoegd wordt. Want het programma STM32CubeIDE zal dit weer resetten en vervolgens zelf 
de kanalen uitkiezen. De code bij het selecteren van kanalen is hieronder:
\\\\
\begin{adjustbox}{width=\textwidth}
    \color{Green}
    \begin{lstlisting}
void ADC1_Select_CH0(void){
ADC_ChannelConfTypeDef sConfig = {0};
/** Configure for the selected ADC regular channel its corresponding 
rank in the sequencer and its sample time.
*/
sConfig.Channel = ADC_CHANNEL_0;
sConfig.Rank = 1;
sConfig.SamplingTime = ADC_SAMPLETIME_28CYCLES_5;
if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK){
    Error_Handler();
    }
}
    \end{lstlisting}
\end{adjustbox}
\\\\
Nadat het mogelijk is om een kanaal te kiezen, moet het selecteren van de poorten nog gebeuren. De poorten worden geselecteerd
in een for-loop, waarbij 4 pinnen aan of uit worden geschakeld. Alle 4 pinnen zijn afhankelijk van de variabel 'i' in combinatie
met de operator 'AND' en een referentiewaarde. De 4 pinnen voor het multiplexen zijn gedefinieerd als S0, S1, S2 en S3. S0 is de LSB
en S3 is uiteraard de MSB. De referentiewaarde waarde van S0 is 1 in binair. Als de variabel 'i' een waarde heeft van 1, welke voor
het selecteren van poort 1 betekent dan wordt poort 1 hoog gezet want:
\[0b0001\ \&\ 0b0001 = \frac{0001}{0001} = 0001\]  
Dit wordt dus toegepast voor de rest van de S1, S2 en S3. Na het selecteren van de poorten, wordt het signaal ingelezen en op volgorde
gezet. Zie de code hieronder:
\\\\
\begin{adjustbox}{width=0.8\textwidth}
    \color{Green}
    \begin{lstlisting}
void selectMux1Pin(){
    ADC1_Select_CH0();
    for(int i=0 ; i<16; i++){
        HAL_GPIO_WritePin(GPIOB, GPIO_PIN_11, SET && (i & 0b00000001));
        HAL_GPIO_WritePin(GPIOB, GPIO_PIN_10, SET && (i & 0b00000010));
        HAL_GPIO_WritePin(GPIOB, GPIO_PIN_1, SET &&  (i & 0b00000100));
        HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0, SET &&  (i & 0b00001000));
        HAL_ADC_Start_DMA(&hadc1, (uint32_t *)&mux.valueMux1[i], 1);
    }
    HAL_ADC_Stop_DMA(&hadc1);
}
    \end{lstlisting}
\end{adjustbox}
\\\\
Bij het testen van de softwarecode is gebleken dat niet alle signalen accuraat zijn. Dit komt omdat niet alle poorten voorzien
zijn van een pull-down weerstand, waardoor de zweverige signalen de overige signalen beïnvloed. Maar door een paar weerstand en 
een signaal van 3V3 op een willekeurige poorten te zetten, zien we dat de geschreven software wel werkt. Bij het testen zijn 
twee pull-down weerstanden op poort 7 en 12 toegepast, en een signaal van 3V3 op poort 15 gezet. Door het bovenstaande instructie
bij het configureren van het STM32CubeIDE in combinatie met de geschreven softwarecode is het resultaat als volgt en te zien in 
\autoref{fig:mux_result}.
\begin{figure}[ht!]
    \includegraphics[scale=0.7]{Detailontwerp/Onderdelen/Afbeelding/MUX/resultaat.png}
    \caption{Het resultaat van de unit Mux}
    \label{fig:mux_result}
\end{figure}

\newpage