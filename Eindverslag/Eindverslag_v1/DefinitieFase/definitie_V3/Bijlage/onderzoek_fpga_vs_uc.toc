\babel@toc {dutch}{}\relax 
\contentsline {section}{\numberline {1}Inleiding}{4}{section.1}%
\contentsline {section}{\numberline {2}Deelvraag 1: Welke hardware kan er gebruikt worden in dit project?}{5}{section.2}%
\contentsline {subsection}{\numberline {2.1}Wat is FPGA?}{5}{subsection.2.1}%
\contentsline {subsubsection}{\numberline {2.1.1}Voordelen}{5}{subsubsection.2.1.1}%
\contentsline {subsubsection}{\numberline {2.1.2}Nadelen}{5}{subsubsection.2.1.2}%
\contentsline {subsection}{\numberline {2.2}Wat is een Microcontroller}{6}{subsection.2.2}%
\contentsline {subsubsection}{\numberline {2.2.1}Hoe werkt een Microcontroller?}{6}{subsubsection.2.2.1}%
\contentsline {subsubsection}{\numberline {2.2.2}Kernelementen van de Microcontroller}{6}{subsubsection.2.2.2}%
\contentsline {subsection}{\numberline {2.3}Conclusie}{7}{subsection.2.3}%
\contentsline {section}{\numberline {3}Deelvraag 2: Hoe wordt data gedownload binnen 60 minuten?}{8}{section.3}%
\contentsline {subsection}{\numberline {3.1}Hoeveel data worden er opgeslagen?}{8}{subsection.3.1}%
\contentsline {subsection}{\numberline {3.2}Interface mogelijkheden}{8}{subsection.3.2}%
\contentsline {subsection}{\numberline {3.3}Conclusie}{9}{subsection.3.3}%
\contentsline {section}{\numberline {4}Deelvraag 3: Hoe kan het systeem zo lang mogelijk blijven werken?}{10}{section.4}%
\contentsline {subsection}{\numberline {4.1}Wat is het stroomverbruik van alle sensoren?}{10}{subsection.4.1}%
\contentsline {subsection}{\numberline {4.2}Welke type voeding is het meest efficiënt?}{10}{subsection.4.2}%
\contentsline {subsubsection}{\numberline {4.2.1}Lineaire voltage regulator}{10}{subsubsection.4.2.1}%
\contentsline {subsubsection}{\numberline {4.2.2}Switching voltage regulator}{11}{subsubsection.4.2.2}%
\contentsline {subsubsection}{\numberline {4.2.3}Hypothetisch van implementatie}{11}{subsubsection.4.2.3}%
\contentsline {subsection}{\numberline {4.3}Conclusie}{13}{subsection.4.3}%
\contentsline {section}{\numberline {5}Hoofdvraag conclusie}{14}{section.5}%
\contentsline {section}{\numberline {6}Bronvermelding}{15}{section.6}%
\contentsline {section}{\numberline {A}Bijlage Buck converter simulatie}{17}{appendix.A}%
\contentsline {section}{\numberline {B}Bijlage Boost converter simulatie}{19}{appendix.B}%
\contentsline {section}{\numberline {C}Eerst Buck en dan Boost simulatie}{21}{appendix.C}%
\contentsline {section}{\numberline {D}Buck-Boost converter simulatie}{22}{appendix.D}%
\contentsline {section}{\numberline {E}Bijlage Buck converter berekening van 7,2V naar 5V}{24}{appendix.E}%
\contentsline {section}{\numberline {F}Bijlage Buck converter berekening van 7,2V naar 3,3V}{26}{appendix.F}%
\contentsline {section}{\numberline {G}Bijlage Buck converter berekening van 4V naar 3,3V}{27}{appendix.G}%
