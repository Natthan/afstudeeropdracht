\babel@toc {dutch}{}
\contentsline {section}{\numberline {1}Inleiding}{4}{section.1}%
\contentsline {section}{\numberline {2}Deelvraag 1: Welke hardware kan er gebruikt worden in dit project?}{5}{section.2}%
\contentsline {subsection}{\numberline {2.1}Wat is FPGA?}{5}{subsection.2.1}%
\contentsline {subsubsection}{\numberline {2.1.1}Voordelen}{5}{subsubsection.2.1.1}%
\contentsline {subsubsection}{\numberline {2.1.2}Nadelen}{5}{subsubsection.2.1.2}%
\contentsline {subsection}{\numberline {2.2}Wat is een Microcontroller}{6}{subsection.2.2}%
\contentsline {subsubsection}{\numberline {2.2.1}Hoe werkt een Microcontroller?}{6}{subsubsection.2.2.1}%
\contentsline {subsubsection}{\numberline {2.2.2}Kernelementen van de Microcontroller}{6}{subsubsection.2.2.2}%
\contentsline {subsection}{\numberline {2.3}Conclusie}{7}{subsection.2.3}%
\contentsline {section}{\numberline {3}Deelvraag 2: Hoe worden de ontvangen data opgeslagen en gedownload?}{7}{section.3}%
\contentsline {subsection}{\numberline {3.1}Hoeveel data worden er opgeslagen?}{7}{subsection.3.1}%
\contentsline {subsection}{\numberline {3.2}Seriële verbinding}{8}{subsection.3.2}%
\contentsline {subsection}{\numberline {3.3}Conclusie}{8}{subsection.3.3}%
\contentsline {section}{\numberline {4}Hoe kan het systeem 24 uur blijven functioneren?}{8}{section.4}%
\contentsline {subsection}{\numberline {4.1}Wat is het stroomverbruik van alle sensoren?}{9}{subsection.4.1}%
\contentsline {subsection}{\numberline {4.2}Welke type voeding is het meest efficënt?}{9}{subsection.4.2}%
\contentsline {subsubsection}{\numberline {4.2.1}Lineaire voltage regulator}{9}{subsubsection.4.2.1}%
\contentsline {section}{\numberline {5}Bronvermelding}{10}{section.5}%
