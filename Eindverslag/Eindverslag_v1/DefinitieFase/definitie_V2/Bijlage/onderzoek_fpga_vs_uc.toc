\babel@toc {dutch}{}
\contentsline {section}{\numberline {1}Inleiding}{4}{section.1}%
\contentsline {section}{\numberline {2}Deelvraag 1: Welke hardware kan er gebruikt worden in dit project?}{5}{section.2}%
\contentsline {subsection}{\numberline {2.1}Wat is FPGA?}{5}{subsection.2.1}%
\contentsline {subsubsection}{\numberline {2.1.1}Voordelen}{5}{subsubsection.2.1.1}%
\contentsline {subsubsection}{\numberline {2.1.2}Nadelen}{5}{subsubsection.2.1.2}%
\contentsline {subsection}{\numberline {2.2}Wat is een Microcontroller}{6}{subsection.2.2}%
\contentsline {subsubsection}{\numberline {2.2.1}Hoe werkt een Microcontroller?}{6}{subsubsection.2.2.1}%
\contentsline {subsubsection}{\numberline {2.2.2}Kernelementen van de Microcontroller}{6}{subsubsection.2.2.2}%
\contentsline {subsection}{\numberline {2.3}Conclusie}{7}{subsection.2.3}%
\contentsline {section}{\numberline {3}Deelvraag 2: Hoe kan het systeem zo lang mogelijk blijven werken?}{8}{section.3}%
\contentsline {subsection}{\numberline {3.1}Hoeveel data worden er opgeslagen?}{8}{subsection.3.1}%
\contentsline {subsection}{\numberline {3.2}Seriële verbinding}{8}{subsection.3.2}%
\contentsline {subsection}{\numberline {3.3}Conclusie}{9}{subsection.3.3}%
\contentsline {section}{\numberline {4}Wat is het stroomverbruik van het systeem?}{9}{section.4}%
\contentsline {subsection}{\numberline {4.1}Wat is het stroomverbruik van alle sensoren?}{9}{subsection.4.1}%
\contentsline {subsection}{\numberline {4.2}Welke type voeding is het meest efficiënt?}{10}{subsection.4.2}%
\contentsline {subsubsection}{\numberline {4.2.1}Lineaire voltage regulator}{10}{subsubsection.4.2.1}%
\contentsline {subsubsection}{\numberline {4.2.2}Switching voltage regulator}{10}{subsubsection.4.2.2}%
\contentsline {subsubsection}{\numberline {4.2.3}Hypothetisch van implementatie}{10}{subsubsection.4.2.3}%
\contentsline {subsection}{\numberline {4.3}Conclusie}{15}{subsection.4.3}%
\contentsline {section}{\numberline {5}Hoofdvraag conclusie}{16}{section.5}%
\contentsline {section}{\numberline {6}Bronvermelding}{17}{section.6}%
\contentsline {section}{\numberline {7}Bijlage Buck converter berekening van 7,2V naar 5V}{19}{section.7}%
\contentsline {section}{\numberline {8}Bijlage buck converter berekening van 7,2V naar 3,3V}{21}{section.8}%
\contentsline {section}{\numberline {9}Bijlage buck converter berekening van 4V naar 3,3V}{22}{section.9}%
