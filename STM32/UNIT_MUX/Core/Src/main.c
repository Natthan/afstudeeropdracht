/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdio.h>
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
typedef struct{
	uint16_t J1, J2, J3, J4, J5, J6, J7 ,J8, J9, J10, J11, J12, J13, J14, J15, J16, // MUX1
		     J17, J18, J19, J20, J21, J22, J23 ,J24, J25, J26, J27, J28, J29, J30, J31, J32, // MUX2
		     J33, J34, J35, J36, J37, J38, J39 ,J40, J41, J42, J43, J44, J45, J46, J47, J48, // MUX3
		     J49, J50, J51, J52, J53, J54, J55 ,J56, J57, J58, J59, J60, J61, J62, J63, J64; // MUX4
} data_mux;
/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
char SaveDataMux1[200] = {'\0'}; // buffer to save data from mux1
uint32_t buffer_mux1[100] = {'\0'}; // temporary buffer to save data from mux1 later to be saved to the variable SaveDataMux1

char SaveDataMux2[200] = {'\0'}; // buffer to save data from mux2
uint32_t buffer_mux2[100] = {'\0'}; // temporary buffer to save data from mux1 later to be saved to the variable SaveDataMux2

char SaveDataMux3[200] = {'\0'}; // buffer to save data from mux3
uint32_t buffer_mux3[100] = {'\0'}; // temporary buffer to save data from mux1 later to be saved to the variable SaveDataMux3

char SaveDataMux4[200] = {'\0'}; // buffer to save data from mux4
uint32_t buffer_mux4[100] = {'\0'}; // temporary buffer to save data from mux1 later to be saved to the variable SaveDataMux4

uint16_t adc_value1;
uint16_t counter1;

uint16_t adc_value2;
uint16_t counter2;

uint8_t mux1[] = "J1 \t\t J2 \t\t J3 \t\t J4 \t\t J5 \t\t J6 \t\t J7 \t\t J8 \t\t J9 \t\t J10 \t\t J11 \t\t J12 \t\t J13 \t\t J14 \t\t J15 \t\t J16\r\n";
uint8_t tab[] = "\t";
uint8_t enter[] = "\n\r";
int Timeout = 1000;
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

data_mux *StructMux1; // struct for mux1
data_mux *StructMux2; // struct for mux2
data_mux *StructMux3; // struct for mux3
data_mux *StructMux4; // struct for mux4

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;
ADC_HandleTypeDef hadc2;

UART_HandleTypeDef huart1;

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_ADC1_Init(void);
static void MX_USART1_UART_Init(void);
static void MX_ADC2_Init(void);
/* USER CODE BEGIN PFP */
void ADC_Select_CH_NUL(void);
void ADC_Select_CH1(void);
void ADC2_Select_CH2(void);
void ADC2_Select_CH3(void);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/*
 * Function to read analog signal from multiplexer 1
 * return none
 */
void updateMux1(){
	static int a = 0; // position variable of the buffer_mux1
	int channel_max = 16; // value of the maximum channel

	ADC_Select_CH_NUL();
	HAL_ADC_Start(&hadc1);
	for(int i=0; i<channel_max; i++){
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0, SET && (i & 0b00000001)); // set gpio pin high with the state of the variable i
		//HAL_Delay(50);
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_1, SET && (i & 0b00000010));
		//HAL_Delay(50);
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_2, SET && (i & 0b00000100));
		//HAL_Delay(50);
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_3, SET && (i & 0b00001000));
		//HAL_Delay(50);
		HAL_ADC_Start(&hadc1); // start reading ADC value
		buffer_mux1[a] = HAL_ADC_GetValue(&hadc1); // get and put value into buffer_mux1 with the position of the variable a
		a = ((a+1)%channel_max); // reset the variable a with the maximum channel
		if(a == channel_max-1){
			StructMux1 = (data_mux *) buffer_mux1; // put the value in the typedef struct, each position represent it's own sensor
			sprintf(SaveDataMux1, "J1: %u | J2: %u | J3: %u | J4: %u | J5: %u | J6: %u | J7: %u | J8: %u "
					"| J9: %u | J10: %u | J11: %u | J12: %u | J13: %u | J14: %u | J15: %u | J16: %u \r\n\r\n",
					StructMux1->J1, StructMux1->J2, StructMux1->J3, StructMux1->J4,
					StructMux1->J5, StructMux1->J6, StructMux1->J7, StructMux1->J8,
					StructMux1->J9, StructMux1->J10, StructMux1->J11, StructMux1->J12,
					StructMux1->J13, StructMux1->J14, StructMux1->J15, StructMux1->J16); // put the value of each sensor in the buffer SaveDataMux1. Each sensor has it's own variable struct
			HAL_UART_Transmit(&huart1, (uint8_t *)SaveDataMux1, sizeof(SaveDataMux1), Timeout); // display via UART
		}
//		buffer_mux1[a] = HAL_ADC_GetValue(&hadc1);
//		a = ((a+1)%channel_max); // reset the variable a with the maximum channel
//		StructMux1 = (data_mux *) buffer_mux1; // put the value in the typedef struct, each position represent it's own sensor
//		sprintf(SaveDataMux1, "J1: %u | J2: %u | J3: %u | J4: %u | J5: %u | J6: %u | J7: %u | J8: %u "
//				"| J9: %u | J10: %u | J11: %u | J12: %u | J13: %u | J14: %u | J15: %u | J16: %u \r\n\r\n",
//				StructMux1->J1, StructMux1->J2, StructMux1->J3, StructMux1->J4,
//				StructMux1->J5, StructMux1->J6, StructMux1->J7, StructMux1->J8,
//				StructMux1->J9, StructMux1->J10, StructMux1->J11, StructMux1->J12,
//				StructMux1->J13, StructMux1->J14, StructMux1->J15, StructMux1->J16); // put the value of each sensor in the buffer SaveDataMux1. Each sensor has it's own variable struct
//		HAL_UART_Transmit(&huart1, (uint8_t *)SaveDataMux1, sizeof(SaveDataMux1), Timeout); // display via UART
	}
}


/*
 * Function to read analog signal from multiplexer 1
 * return none
 */
void updateMux2(){
	static int a = 0;
	int channel_max = 16;

	ADC_Select_CH1();
	HAL_ADC_Start(&hadc1);
	for(int i=0; i<channel_max; i++){
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, SET && (i & 0b00000001));
		//HAL_Delay(50);
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_9, SET && (i & 0b00000010));
		//HAL_Delay(50);
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_10, SET && (i & 0b00000100));
		//HAL_Delay(50);
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_11, SET && (i & 0b00001000));
		//HAL_Delay(50);
		HAL_ADC_Start(&hadc1);
		buffer_mux2[a] = HAL_ADC_GetValue(&hadc1);
		a = ((a+1)%channel_max);
		if(a == channel_max-1){
			StructMux2 = (data_mux *) buffer_mux2;
			sprintf(SaveDataMux2, "J17:%u | J18: %u | J19: %u | J20: %u | J21: %u |  J22: %u | J23: %u | J24: %u "
					"| J25: %u | J26: %u | J27: %u | J28: %u | J29: %u | J30: %u | J31: %u | J32: %u \r\n\r\n",
					StructMux2->J17, StructMux2->J18, StructMux2->J19, StructMux2->J20,
					StructMux2->J21, StructMux2->J22, StructMux2->J23, StructMux2->J24,
					StructMux2->J25, StructMux2->J26, StructMux2->J27, StructMux2->J28,
					StructMux2->J29, StructMux2->J30, StructMux2->J31, StructMux2->J32);
			HAL_UART_Transmit(&huart1, (uint8_t *)SaveDataMux2, sizeof(SaveDataMux2), Timeout);
		}
	}
}


/*
 * Function to read analog signal from multiplexer 1
 * return none
 */
void updateMux3(){
	static int a = 0;
	int channel_max = 16;

	ADC2_Select_CH2();
	for(int i=0; i<channel_max; i++){
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_4, SET && (i & 0b00000001));
		//HAL_Delay(50);
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5, SET && (i & 0b00000010));
		//HAL_Delay(50);
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_6, SET && (i & 0b00000100));
		//HAL_Delay(50);
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_7, SET && (i & 0b00001000));
		//HAL_Delay(50);
		HAL_ADC_Start(&hadc2);
		buffer_mux3[a] = HAL_ADC_GetValue(&hadc2);
		a = ((a+1)%channel_max);
		if(a == channel_max-1){
			StructMux3 = (data_mux *) buffer_mux3;
			sprintf(SaveDataMux3, "J33: %u |  J34: %u | J35: %u | J36: %u | J37: %u | J38: %u | J39: %u | J40: %u "
					"| J41: %u | J42: %u | J43: %u | J44: %u | J45: %u | J46: %u | J47: %u | J48: %u\r\n\r\n",
					StructMux3->J33, StructMux3->J34, StructMux3->J35, StructMux3->J36,
					StructMux3->J37, StructMux3->J38, StructMux3->J39, StructMux3->J40,
					StructMux3->J41, StructMux3->J42, StructMux3->J43, StructMux3->J44,
					StructMux3->J45, StructMux3->J46, StructMux3->J47, StructMux3->J48);
			HAL_UART_Transmit(&huart1, (uint8_t *)SaveDataMux3, sizeof(SaveDataMux3), Timeout);
		}
	}
}


/*
 * Function to read analog signal from multiplexer 1
 * return none
 */
void updateMux4(){
	static int a = 0;
	int channel_max = 16;

	ADC2_Select_CH3();
	for(int i=0; i<channel_max; i++){
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12, SET && (i & 0b00000001));
		//HAL_Delay(50);
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_13, SET && (i & 0b00000010));
		//HAL_Delay(50);
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_14, SET && (i & 0b00000100));
		//HAL_Delay(50);
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_15, SET && (i & 0b00001000));
		//HAL_Delay(50);
		a = ((a+1)%channel_max);
		if(a == channel_max-1){
			StructMux4 = (data_mux *) buffer_mux4;
			sprintf(SaveDataMux4, "J49: %u | J50: %u | J51: %u | J52: %u | J53: %u | J54: %u | J55 : %u | J56: %u "
					"| J57: %u | J58: %u | J59: %u | J60: %u | J61: %u | J62: %u | J63: %u | J64: %u \r\n\r\n",
					StructMux4->J49, StructMux4->J50, StructMux4->J51, StructMux4->J52,
					StructMux4->J53, StructMux4->J54, StructMux4->J55, StructMux4->J56,
					StructMux4->J57, StructMux4->J58, StructMux4->J59, StructMux4->J60,
					StructMux4->J61, StructMux4->J62, StructMux4->J63, StructMux4->J64);
			HAL_UART_Transmit(&huart1, (uint8_t *)SaveDataMux4, sizeof(SaveDataMux4), Timeout);
		}
	}
}

void ADC_Select_CH_NUL(void){
	ADC_ChannelConfTypeDef sConfig = {0};
	sConfig.Channel = ADC_CHANNEL_0;
	sConfig.Rank = 1;
	sConfig.SamplingTime = ADC_SAMPLETIME_1CYCLE_5;
	if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
	{
	Error_Handler();
	}
}

void ADC_Select_CH1(void){
	ADC_ChannelConfTypeDef sConfig = {0};
	sConfig.Channel = ADC_CHANNEL_1;
	sConfig.Rank = 1;
	sConfig.SamplingTime = ADC_SAMPLETIME_1CYCLE_5;
	if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
	{
	Error_Handler();
	}
}

void ADC2_Select_CH2(void){
	ADC_ChannelConfTypeDef sConfig = {0};
	sConfig.Channel = ADC_CHANNEL_2;
	sConfig.Rank = ADC_REGULAR_RANK_1;
	sConfig.SamplingTime = ADC_SAMPLETIME_1CYCLE_5;
	if (HAL_ADC_ConfigChannel(&hadc2, &sConfig) != HAL_OK)
	{
	Error_Handler();
	}
}

void ADC2_Select_CH3(void){
	ADC_ChannelConfTypeDef sConfig = {0};
	sConfig.Channel = ADC_CHANNEL_3;
	sConfig.Rank = ADC_REGULAR_RANK_1;
	sConfig.SamplingTime = ADC_SAMPLETIME_1CYCLE_5;
	if (HAL_ADC_ConfigChannel(&hadc2, &sConfig) != HAL_OK)
	{
	Error_Handler();
	}
}

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_ADC1_Init();
  MX_USART1_UART_Init();
  MX_ADC2_Init();
  /* USER CODE BEGIN 2 */
  HAL_ADC_Start(&hadc1);
  HAL_ADC_Start(&hadc2);
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
	  //updateMux1();
	  //HAL_Delay(500);
	  //updateMux2();
	  //HAL_Delay(500);
	  //updateMux3();
	  //HAL_Delay(500);
	  updateMux4();
	  //HAL_Delay(500);
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
	  HAL_Delay(500);
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_ADC;
  PeriphClkInit.AdcClockSelection = RCC_ADCPCLK2_DIV2;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief ADC1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC1_Init(void)
{

  /* USER CODE BEGIN ADC1_Init 0 */

  /* USER CODE END ADC1_Init 0 */

  //ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC1_Init 1 */

  /* USER CODE END ADC1_Init 1 */
  /** Common config
  */
  hadc1.Instance = ADC1;
  hadc1.Init.ScanConvMode = ADC_SCAN_ENABLE;
  hadc1.Init.ContinuousConvMode = ENABLE;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.NbrOfConversion = 2;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Regular Channel
  */
//  sConfig.Channel = ADC_CHANNEL_0;
//  sConfig.Rank = ADC_REGULAR_RANK_1;
//  sConfig.SamplingTime = ADC_SAMPLETIME_1CYCLE_5;
//  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
//  {
//    Error_Handler();
//  }
//  /** Configure Regular Channel
//  */
//  sConfig.Channel = ADC_CHANNEL_1;
//  sConfig.Rank = ADC_REGULAR_RANK_2;
//  sConfig.SamplingTime = ADC_SAMPLETIME_7CYCLES_5;
//  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
//  {
//    Error_Handler();
//  }
  /* USER CODE BEGIN ADC1_Init 2 */

  /* USER CODE END ADC1_Init 2 */

}

/**
  * @brief ADC2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC2_Init(void)
{

  /* USER CODE BEGIN ADC2_Init 0 */

  /* USER CODE END ADC2_Init 0 */

  //ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC2_Init 1 */

  /* USER CODE END ADC2_Init 1 */
  /** Common config
  */
  hadc2.Instance = ADC2;
  hadc2.Init.ScanConvMode = ADC_SCAN_ENABLE;
  hadc2.Init.ContinuousConvMode = ENABLE;
  hadc2.Init.DiscontinuousConvMode = DISABLE;
  hadc2.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc2.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc2.Init.NbrOfConversion = 2;
  if (HAL_ADC_Init(&hadc2) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Regular Channel
  */
//  sConfig.Channel = ADC_CHANNEL_2;
//  sConfig.Rank = ADC_REGULAR_RANK_1;
//  sConfig.SamplingTime = ADC_SAMPLETIME_1CYCLE_5;
//  if (HAL_ADC_ConfigChannel(&hadc2, &sConfig) != HAL_OK)
//  {
//    Error_Handler();
//  }
//  /** Configure Regular Channel
//  */
//  sConfig.Channel = ADC_CHANNEL_3;
//  sConfig.Rank = ADC_REGULAR_RANK_2;
//  if (HAL_ADC_ConfigChannel(&hadc2, &sConfig) != HAL_OK)
//  {
//    Error_Handler();
//  }
  /* USER CODE BEGIN ADC2_Init 2 */

  /* USER CODE END ADC2_Init 2 */

}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 115200;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_10
                          |GPIO_PIN_11|GPIO_PIN_12|GPIO_PIN_13|GPIO_PIN_14
                          |GPIO_PIN_15|GPIO_PIN_3|GPIO_PIN_4|GPIO_PIN_5
                          |GPIO_PIN_6|GPIO_PIN_7|GPIO_PIN_8|GPIO_PIN_9, GPIO_PIN_RESET);

  /*Configure GPIO pin : PC13 */
  GPIO_InitStruct.Pin = GPIO_PIN_13;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : PB0 PB1 PB2 PB10
                           PB11 PB12 PB13 PB14
                           PB15 PB3 PB4 PB5
                           PB6 PB7 PB8 PB9 */
  GPIO_InitStruct.Pin = GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_10
                          |GPIO_PIN_11|GPIO_PIN_12|GPIO_PIN_13|GPIO_PIN_14
                          |GPIO_PIN_15|GPIO_PIN_3|GPIO_PIN_4|GPIO_PIN_5
                          |GPIO_PIN_6|GPIO_PIN_7|GPIO_PIN_8|GPIO_PIN_9;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

