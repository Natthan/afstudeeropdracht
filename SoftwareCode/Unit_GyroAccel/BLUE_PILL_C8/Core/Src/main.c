/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdio.h>
#include <inttypes.h>
#include <stdbool.h>
#include <string.h>
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
typedef struct{
	/* MPU6050 registers */
	char MPU6050_ADDR;
	char WHO_AM_I_REG;
	char SMPLRT_DIV_REG;
	char GYRO_CONFIG_REG;
	char ACCEL_CONFIG_REG;
	char ACCEL_XOUT_H_REG;
	char TEMP_OUT_H_REG;
	char GYRO_XOUT_H_REG;
	char PWR_MGMT_1_REG;

	/* accelerometer variables */
	int16_t Accel_x_raw;
	int16_t Accel_y_raw;
	int16_t Accel_z_raw;
	double Ax;
	double Ay;
    double Az;

    /* gyroscope variables */
	int16_t Gyro_x_raw;
	int16_t Gyro_y_raw;
	int16_t Gyro_z_raw;
	double Gx;
	double Gy;
	double Gz;

	uint8_t check, data, rx_char_gyroaccel;
	uint8_t rec_data[6]; // to read the data we need to read 6 bytes
	char buf[50]; // buffer to store data for uart
	char GetCommandFromUart[15];

	char *command, *arg;

	int off;
} GyroAcc;

GyroAcc gyroaccel;
/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
int Timeout = 1000;
int Delay = 1000;

uint8_t enter[] = "\r\n";
uint8_t tab[] = "\t";

int a = 0;
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
I2C_HandleTypeDef hi2c1;
I2C_HandleTypeDef hi2c2;

UART_HandleTypeDef huart1;

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_USART1_UART_Init(void);
static void MX_I2C1_Init(void);
static void MX_I2C2_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
void send_uart (char *string)
{
	uint8_t len = strlen(string);
	HAL_UART_Transmit(&huart1, (uint8_t *)string, len, Timeout);  // transmit in blocking mode huart1
}

/*
 * Config MPU6050
 * no return
 */
void MPU6050_init(void){
	gyroaccel.MPU6050_ADDR = 0xD0;
	gyroaccel.WHO_AM_I_REG = 0x75;
	gyroaccel.SMPLRT_DIV_REG = 0x19;
    gyroaccel.GYRO_CONFIG_REG = 0x1B;
	gyroaccel.ACCEL_CONFIG_REG = 0x1C;
	gyroaccel.ACCEL_XOUT_H_REG = 0x3B;
	gyroaccel.TEMP_OUT_H_REG = 0x41;
	gyroaccel.GYRO_XOUT_H_REG = 0x43;
	gyroaccel.PWR_MGMT_1_REG = 0x6B;

	HAL_I2C_Mem_Read(&hi2c2, gyroaccel.MPU6050_ADDR, gyroaccel.WHO_AM_I_REG, 1, &gyroaccel.check, 1, Timeout); // send who_am_i value (0x75) to get a response from MPU6050

	if(gyroaccel.check == 104){ // if response is 0x68 == 104, device is ready
		gyroaccel.data = 0; // set data to send to 0x00;
		HAL_I2C_Mem_Write(&hi2c2, gyroaccel.MPU6050_ADDR, gyroaccel.PWR_MGMT_1_REG, 1, &gyroaccel.data, 1, Timeout); // send 0 to pwr_mgmt_1 register (0x6b) to wake the sensor up

		gyroaccel.data = 0x07; // set data to send to 7
		HAL_I2C_Mem_Write(&hi2c2, gyroaccel.MPU6050_ADDR, gyroaccel.SMPLRT_DIV_REG, 1, &gyroaccel.data, 1, Timeout); // init sample rate of 1Khz by sending 7 to smplrt_div (0x19) register

		gyroaccel.data = 0x18; // set data to send to 0 to config 250 d/s, data = 0x18 ==> 2000 d/s
		HAL_I2C_Mem_Write(&hi2c2, gyroaccel.MPU6050_ADDR, gyroaccel.GYRO_CONFIG_REG, 1, &gyroaccel.data, 1, Timeout); // config full scale range of gyroscope by sending 0 to gyro_config (0x1b) register

		gyroaccel.data = 0x0; // set data to send to 0 to config 2g, data = 0x18 ==> 16g
		HAL_I2C_Mem_Write(&hi2c2, gyroaccel.MPU6050_ADDR, gyroaccel.ACCEL_CONFIG_REG, 1, &gyroaccel.data, 1, Timeout); // config full scale range of accelerometer by sending 0 to accel_config (0x1c) register
	}
	else{
		uint8_t errormsg[] = "The MPU6050 did not response with 104 or 0x68\r\n";
		HAL_UART_Transmit(&huart1, errormsg, sizeof(errormsg), Timeout);
	}
}

/*
 * Get value from 3-axis gyroscope and accelerometer
 * no return
 * print the value immediately
 */
void GetValueFromGyroAndAccel(void){
	/* Get accel value */
	HAL_I2C_Mem_Read(&hi2c2, gyroaccel.MPU6050_ADDR, gyroaccel.ACCEL_XOUT_H_REG, 1, gyroaccel.rec_data, 6, Timeout); // read 6 bytes from accel_xout_h_reg register

	/*
	 * these 6 bytes of data will be extract into three 16 bits integer values
	 * raw_data = (accel_xout_HighBit << 8 | accel_xout_LowBit)
	 * we are shifting the higher 8 bits to the left and or it with the lower 8 bits
	 */
	gyroaccel.Accel_x_raw = (int16_t)(gyroaccel.rec_data[0] << 8 | gyroaccel.rec_data[1]); // the data output for each axis will be a signed 16 bit integer
	gyroaccel.Accel_y_raw = (int16_t)(gyroaccel.rec_data[2] << 8 | gyroaccel.rec_data[3]); // the data output for each axis will be a signed 16 bit integer
	gyroaccel.Accel_z_raw = (int16_t)(gyroaccel.rec_data[4] << 8 | gyroaccel.rec_data[5]); // the data output for each axis will be a signed 16 bit integer

	/*
	 * to get these raw value into 'g' value
	 * at 2g full scale the raw data need to divide with 16384 LSB/g
	 */
	gyroaccel.Ax = gyroaccel.Accel_x_raw/16384.0; // 16384.0 is important to get float values
	gyroaccel.Ay = gyroaccel.Accel_y_raw/16384.0; // 2048.0 if full scale range is 16g
	gyroaccel.Az = gyroaccel.Accel_z_raw/16384.0;

	sprintf(gyroaccel.buf, "Ax: %.2f | Ay: %.2f | Az: %.2f", gyroaccel.Ax, gyroaccel.Ay, gyroaccel.Az); // store data to buf to display in uart
	HAL_UART_Transmit(&huart1, (uint8_t *)&gyroaccel.buf, sizeof(gyroaccel.buf), Timeout); // display in uart
	HAL_UART_Transmit(&huart1, tab, sizeof(tab), Timeout); // tab function to put the next value next the old value


	/* Get gyro value */
	HAL_I2C_Mem_Read(&hi2c2, gyroaccel.MPU6050_ADDR, gyroaccel.GYRO_XOUT_H_REG, 1, gyroaccel.rec_data, 6, Timeout); // read 6 bytes from gyro_xout_h_reg register

	/*
	 * these 6 bytes of data will be extract into three 16 bits integer values
	 * raw_data = (gyro_out_HighBit << 8 | gyro_xout_LowBit)
	 * we are shifting the higher 8 bits to the left and or it with the lower 8 bits
	 */
	gyroaccel.Gyro_x_raw = (int16_t)(gyroaccel.rec_data[0] << 8 | gyroaccel.rec_data[1]); // the data output for each axis will be a signed 16 bit integer
	gyroaccel.Gyro_y_raw = (int16_t)(gyroaccel.rec_data[2] << 8 | gyroaccel.rec_data[3]); // the data output for each axis will be a signed 16 bit integer
	gyroaccel.Gyro_z_raw = (int16_t)(gyroaccel.rec_data[4] << 8 | gyroaccel.rec_data[5]); // the data output for each axis will be a signed 16 bit integer

	/*
	 * get raw data to d/s
	 * by 250 d/s the sensitivity is given at 131 LSB/dps
	 */
	gyroaccel.Gx = gyroaccel.Gyro_x_raw/131.0; // 131.0 is important to get float values
	gyroaccel.Gy = gyroaccel.Gyro_y_raw/131.0; // 16.4 is full scale range is 2000 d/s
	gyroaccel.Gz = gyroaccel.Gyro_z_raw/131.0;

	sprintf(gyroaccel.buf, "Gx: %.2f | Gy: %.2f | Gz: %.2f", gyroaccel.Gx, gyroaccel.Gy, gyroaccel.Gz); // store data to buf to display in uart
	HAL_UART_Transmit(&huart1, (uint8_t *)&gyroaccel.buf, sizeof(gyroaccel.buf), Timeout); // display in uart
	HAL_UART_Transmit(&huart1, (uint8_t *)&enter, sizeof(enter), Timeout); // enter function in uart so the next value will be under the old value
}

void MPU6050_accel(void){
	uint8_t rec_data[6];
	char buf[50] = {0};
	HAL_I2C_Mem_Read(&hi2c2, gyroaccel.MPU6050_ADDR, gyroaccel.ACCEL_XOUT_H_REG, 1, rec_data, 6, Timeout);

	gyroaccel.Accel_x_raw = (int16_t)(rec_data[0] << 8 | rec_data[1]);
	gyroaccel.Accel_y_raw = (int16_t)(rec_data[2] << 8 | rec_data[3]);
	gyroaccel.Accel_z_raw = (int16_t)(rec_data[4] << 8 | rec_data[5]);

	gyroaccel.Ax = gyroaccel.Accel_x_raw/16386.0;
	gyroaccel.Ay = gyroaccel.Accel_y_raw/16386.0;
	gyroaccel.Az = gyroaccel.Accel_z_raw/16386.0;

	sprintf(buf, "Ax: %.2f | Ay: %.2f | Az: %.2f", gyroaccel.Ax, gyroaccel.Ay, gyroaccel.Az);
	HAL_UART_Transmit(&huart1, (uint8_t *)&buf, sizeof(buf), Timeout);
	HAL_UART_Transmit(&huart1, tab, sizeof(tab), Timeout);
}

void MPU6050_gyro(void){
	uint8_t rec_data[6];
	char buf[50] = {0};

	HAL_I2C_Mem_Read(&hi2c2, gyroaccel.MPU6050_ADDR, gyroaccel.GYRO_XOUT_H_REG, 1, rec_data, 6, Timeout);

	gyroaccel.Gyro_x_raw = (int16_t)(rec_data[0] << 8 | rec_data[1]);
	gyroaccel.Gyro_y_raw = (int16_t)(rec_data[2] << 8 | rec_data[3]);
	gyroaccel.Gyro_z_raw = (int16_t)(rec_data[4] << 8 | rec_data[5]);

	gyroaccel.Gx = gyroaccel.Gyro_x_raw/131.0;
	gyroaccel.Gy = gyroaccel.Gyro_y_raw/131.0;
	gyroaccel.Gz = gyroaccel.Gyro_z_raw/131.0;

	sprintf((char* )buf, "Gx: %.2f | Gy: %.2f | Gz: %.2f", gyroaccel.Gx, gyroaccel.Gy, gyroaccel.Gz);
	HAL_UART_Transmit(&huart1, (uint8_t *)&buf, sizeof(buf), Timeout);
	HAL_UART_Transmit(&huart1, (uint8_t *)&enter, sizeof(enter), Timeout);
}

void ExtractInput(char CheckCommandAndArg[]){
	gyroaccel.command = strtok(CheckCommandAndArg, " "); // get first argument from buffer
	send_uart((char *)gyroaccel.command);
	if(strcasecmp(gyroaccel.command, "gyroacc\0") == 0){
		gyroaccel.arg = strtok(NULL, " "); // get second argument from buffer
		send_uart((char *)gyroaccel.arg);
		if(strcasecmp(gyroaccel.arg, "on\r") == 0){
			send_uart("\r\nGyroAccel is turned on!\r\n");
			gyroaccel.off = 0;
		}
		else if(strcasecmp(gyroaccel.arg, "off\r") == 0){
			send_uart("\r\nGyroAccel is turned off!\r\n");
			gyroaccel.off = 1;
		}
	}

	else if(gyroaccel.off == 0 && strcasecmp(gyroaccel.command, "read\r") == 0){
		while(gyroaccel.rx_char_gyroaccel != 32){
			HAL_UART_Receive_IT(&huart1, &gyroaccel.rx_char_gyroaccel, 1); // check constant if interrupt occured
			GetValueFromGyroAndAccel();
		}
	}
	else if(strcasecmp(gyroaccel.command, "gyroacc\0") != 0){
		send_uart("\r\nCommand not founded!\r\n");
		send_uart("Typed command: \t");
		send_uart(gyroaccel.command);
		send_uart("\r\nTry gyroacc <argument> or read if the unit is turned on\r\n");
	}
	else if(gyroaccel.off == 1){
		send_uart("The unit gyroaccel might be turned off!\r\n");
	}
}

void GetDataFromUart(void){
	HAL_UART_Receive_IT(&huart1, &gyroaccel.rx_char_gyroaccel, 1); // check constant if interrupt occured

	if(gyroaccel.rx_char_gyroaccel != 0){
		HAL_UART_Receive_IT(&huart1, &gyroaccel.rx_char_gyroaccel, 1); // check constant if interrupt occured
		gyroaccel.GetCommandFromUart[a++] = gyroaccel.rx_char_gyroaccel; // put this character in buffer
		HAL_UART_Transmit(&huart1, &gyroaccel.rx_char_gyroaccel, sizeof(gyroaccel.rx_char_gyroaccel), Timeout); // echo the given character to the user
		if(gyroaccel.rx_char_gyroaccel == 13){ // if enter is pressed in uart
			ExtractInput(gyroaccel.GetCommandFromUart);
			gyroaccel.rx_char_gyroaccel = 0x00;
			memset(gyroaccel.GetCommandFromUart, 0, sizeof(gyroaccel.GetCommandFromUart));
			a=0;
			send_uart("\r\n");
		}
		gyroaccel.rx_char_gyroaccel = 0;

	}
}

void HAL_USART_RxCpltCallback(UART_HandleTypeDef *huart){
	HAL_UART_Receive_IT(&huart1, &gyroaccel.rx_char_gyroaccel, 1); // receive data from uart when interrupt occured
//	  if(gyroaccel.rx_char_gyroaccel == 32){
//		  GetValueFromGyroAndAccel();
//	  }
}
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART1_UART_Init();
  MX_I2C1_Init();
  MX_I2C2_Init();
  /* USER CODE BEGIN 2 */
  gyroaccel.off = 1;
  uint8_t msg[] = "Unit gyroscope and accelerometer started!\r\n";
  HAL_UART_Transmit(&huart1, msg, sizeof(msg), Timeout);
  send_uart("The unit gyroscope is turned off!\r\n");
  send_uart("Press spacebar to leave reading loop!\r\n");
  send_uart("Type read to get the value from the unit in a while-loop\r\n");
  send_uart("Type gyroacc on or off the unit\r\n");
  MPU6050_init();
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
//	  MPU6050_accel();
//	  MPU6050_gyro();
//	  HAL_UART_Receive_IT(&huart1, &gyroaccel.rx_char_gyroaccel, 1); // check constant if interrupt occured
//	  if(gyroaccel.rx_char_gyroaccel != 0){
//		  while(gyroaccel.rx_char_gyroaccel != 0){
//			  GetDataFromUart();
//		  }
//	  }
	  //GetValueFromGyroAndAccel();

	  GetDataFromUart();
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
	  //HAL_Delay(Delay);
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.ClockSpeed = 100000;
  hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief I2C2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C2_Init(void)
{

  /* USER CODE BEGIN I2C2_Init 0 */

  /* USER CODE END I2C2_Init 0 */

  /* USER CODE BEGIN I2C2_Init 1 */

  /* USER CODE END I2C2_Init 1 */
  hi2c2.Instance = I2C2;
  hi2c2.Init.ClockSpeed = 100000;
  hi2c2.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c2.Init.OwnAddress1 = 0;
  hi2c2.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c2.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c2.Init.OwnAddress2 = 0;
  hi2c2.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c2.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C2_Init 2 */

  /* USER CODE END I2C2_Init 2 */

}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 9600;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, GPIO_PIN_RESET);

  /*Configure GPIO pin : PC13 */
  GPIO_InitStruct.Pin = GPIO_PIN_13;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
